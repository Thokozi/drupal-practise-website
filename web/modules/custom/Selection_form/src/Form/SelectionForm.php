<?php

namespace Drupal\selection_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SelectionForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static();
  }

  public function getFormId()
  {
    // TODO: Implement getFormId() method.
    return 'selection_form';
  }

  /**
   * Build the simple form.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    function converttosecs ($time_string)
    {
      if (strlen($time_string) == 5) {
        $get_hour = intval(substr($time_string, -5, -3));
        $get_mins = intval(substr($time_string, -2));
        $timestamp = ($get_hour * 3600) + ($get_mins * 60);
      } elseif (strlen($time_string) == 4) {
        $get_hour = intval(substr($time_string, -4, -3));
      $get_mins = intval(substr($time_string, -2));
        $timestamp = ($get_hour * 3600) + ($get_mins * 60);
      } elseif (is_int($time_string) == 1) {
        $timestamp = ($time_string * 60);
      }
      return $timestamp;
    }


    function get_hours_range( $start, $end, $step, $format ) {/** @var $start @var $end are the time range in a string format and the $step variable is an integer representing the seconds  */
      $times = array();
      $start_time = converttosecs($start);
      $end_time = converttosecs($end);
      $step_time = converttosecs($step);
      foreach ( range( $start_time, $end_time, $step_time ) as $timestamp ) {
        $hour_mins = gmdate( 'H:i', $timestamp );
        if ( ! empty( $format ) )
          $times[] = gmdate( $format, $timestamp );
        else $times[] = $hour_mins;
      }
      return $times;
    }


    $work_time = get_hours_range('09:00', '11:59', 1, 'H:i');
    $lunch = get_hours_range('12:00', '15:59', 1, 'H:i');
    $home_time = get_hours_range('16:00', '18:00', 1, 'H:i');


    array_unshift($work_time,"none");
    array_unshift($lunch,"none");
    array_unshift($home_time,"none");

    $form['work'] = [
      '#type' => 'select',
      '#title' => $this->t('Set Work Time'),
      '#options' => $work_time,
      '#default_value' => 'none',
    ];

    $form['lunch'] = [
      '#type' => 'select',
      '#title' => $this->t('Set Lunch Time'),
      '#options' => $lunch,
      '#default_value' => 'none',
    ];

    $form['home'] = [
      '#type' => 'select',
      '#title' => $this->t('Set Home Time'),
      '#options' => $home_time,
      '#default_value' => 'none',
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Set Reminders'),
    ];
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    $work = $form_state->getValue('work');

    if ($work == NULL) {
      $form_state->setErrorByName('work',
      $this->t('Please select a time'));
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {

    $work_key = $form_state->getValue('work');
    $work = $form['work']['#options'][$work_key];


    $lunch_key = $form_state->getValue('lunch');
    $lunch = $form['lunch']['#options'][$lunch_key];


    $home_key = $form_state->getValue('home');
    $home = $form['home']['#options'][$home_key];

    $this->messenger()->addStatus($this->t('You have selected work time: %work, lunch time: %lunch and home time: %home.', ['%work' => $work, '%lunch' => $lunch, '%home' => $home]));

    $connection = \Drupal::service('database');
    $query = $connection->insert('selection_data')
      ->fields([
        'work' => $work,
        'lunch' => $lunch,
        'home' => $home,
      ])
      ->execute();


  }
}
