<?php

namespace Drupal\selection_form\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'SelectionBlock' block.
 *
 * @Block(
 *   id = "selection_block",
 *   admin_label = @Translation("Selection block"),
 *   category = @Translation("Custom blocks")
 * )
 */

class SelectionBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $form = \Drupal::formBuilder()->getForm('Drupal\selection_form\Form\SelectionForm');

    return $form;
  }
}
